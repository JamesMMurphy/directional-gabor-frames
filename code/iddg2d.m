%% DDG2D Apply the inverse discrete directional Gabor (ddg) transform to a given coefficents with window function (g), paramter set (lambda), and original dimensions (dims)
%
% Input
%    coeffs (vector): A vector of length lambda.size, it represents the original
%       image under the ddg transform.
%    g (function): The window function g
%    lambda (struct): The parameter set lambda
%    dims (vector): The size (in pixels) of the original image
%
% Output
%    f (N x M matrix): An image with the same dimensions as dims whose ddg transform
%       is equal to coeffs.

%   Copyright 2016 Wojciech Czaja, Benjamin Manning, James Murphy, Kevin Stubbs

function f = iddg2d(coeffs, g, lambda, dims)
if(nargin ~= 4)
    error('Error, wrong number of arguments');
end
% Let S be the analysis operator, our input is Sf so here we calculate
% (S^*S)f
syn_coeffs = analysis_synthesis_op(coeffs, g, lambda, dims, 'synthesis');

% Perform conjugate gradient to invert the frame operator = S^*S. Since we
% calculated (S^*S)f before, this line will calculate (S^*S)^(-1)(S^*S)f =
% f
soln = pcg(@frame_op, syn_coeffs);
    function output = frame_op(in)
        tmp = analysis_synthesis_op(in, g, lambda, dims, 'analysis');
        output = analysis_synthesis_op(tmp, g, lambda, dims, 'synthesis');
    end

% Reshape the output to match the original picture
f = reshape(soln, dims);
end

