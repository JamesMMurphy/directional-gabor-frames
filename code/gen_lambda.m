%% GEN_LAMBDA Generate parameter set for use in the ddg2d and iddg2d routines.
%
% Input
%    type (string): The type of the parameter set to generate, currently
%       supported types are 'square' and 'specify'.
%          'square' type: A square sublattice of Z^2
%          'specify' type: Specific points in Z^2.
%    ranges (cell, contents depends on type): This cell contains all of the
%       information needed to define a parameter set of the given type.
%          'square' type: ranges is a cell with three elements, ranges{1}
%             holds the t values, ranges{2} holds the x values of the
%             sublattice and ranges{3} holds the y values of the sublattice.
%          'specify' type: ranges is a cell with two elements. ranges{1} holds
%             a list of t values, ranges{2} is an N x 2 array where the each
%             row is an ordered pair (x, y) in Z^2.
%
% Output
%    lambda (struct): a structure representing the parameter set. It is
%       guaranteed to have two fields, 'size' and 'get'.
%         lambda.size: The number of parameters in this set.
%         lambda.get: A reference to a function which takes an integer from
%            1 to lambda.size and returns the parameter associated with the
%            given number.

%   Copyright 2016 Wojciech Czaja, Benjamin Manning, James Murphy, Kevin Stubbs
function lambda = gen_lambda(type, ranges)

switch type
    case 'specify'
        lambda = struct();
        lambda.type = type;
        lambda.t_values = ranges{1};
        lambda.points = ranges{2};
        lambda.size = length(ranges{1}) * size(ranges{2}, 1);
        lambda.get = @(n)(get_pts(n, lambda));
    case 'square'
        lambda = struct();
        lambda.type = type;
        lambda.t_range = ranges{1};
        lambda.x_range = ranges{2};
        lambda.y_range = ranges{3};
        lambda.size = length(ranges{1}) * ...
            length(ranges{2}) * ...
            length(ranges{3});
        lambda.get = @(n)(get_square(n, lambda));
    otherwise
        error('Error, unsupported type');
end
end

function [t, x, y] = get_pts(num, lambda)
%% -1 is because MATLAB arrays start at 1
num = num - 1;

t_len = length(lambda.t_values);
pts_len = size(lambda.points, 1);

rem1 = floor(num  / t_len);
rem2 = floor(rem1 / pts_len);

%% +1 is because MATLAB arrays start at 1
t_idx   = num  - t_len * rem1 + 1;
pts_idx = rem1 - x_len * rem2 + 1;

t = lambda.t_range(t_idx);
x = lambda.points(pts_idx, 1);
y = lambda.points(pts_idx, 2);
end

function [t, x, y] = get_square(num, lambda)
%% -1 is because MATLAB arrays start at 1
num = num - 1;

t_len = length(lambda.t_range);
x_len = length(lambda.x_range);
y_len = length(lambda.y_range);

rem1 = floor(num  / t_len);
rem2 = floor(rem1 / x_len);
rem3 = floor(rem2 / y_len);

%% +1 is because MATLAB arrays start at 1
t_idx = num  - t_len * rem1 + 1;
x_idx = rem1 - x_len * rem2 + 1;
y_idx = rem2 - y_len * rem3 + 1;

t = lambda.t_range(t_idx);
x = lambda.x_range(x_idx);
y = lambda.y_range(y_idx);
end
