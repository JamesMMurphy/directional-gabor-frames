%% ANALYSIS_SYNTHESIS_OP Apply the discrete Gabor system's frame operator to the input ('analysis') or its dual ('synthesis')
%
% Input
%    input (vector): An N x 1 vector to apply the frame operator or its
%       dual to. If performing 'analysis' N must equal prod(dims). If
%       performing 'synthesis' N must equal lambda.size.
%    g (function): The window function g
%    lambda (struct): The parameter set lambda
%    dims (vector): The dimensions of the original image which you are
%       using the discrete Gabor system to analyze.
%    mode (string): Either 'analysis' or 'synthesis' (for the frame
%       operator its dual respectively).
%    step (integer): The number of coeffiecents to evaluate each loop
%       (see documentation for details).
%
% Output
%    output (vector): The frame operator or its dual applied to the input.

%   Copyright 2016 Wojciech Czaja, Benjamin Manning, James Murphy, Kevin Stubbs
function output = analysis_synthesis_op(input, g, lambda, dims, mode, step)
if(nargin == 5)
    step = 1000; % A completely arbitrary choice, can change to any positive integer
end

switch mode
    case 'analysis'
        % The size of our input must input must match the passed size
        assert(numel(input) == prod(dims));
        output = zeros(lambda.size, 1);
    case 'synthesis'
        % The size of our input must input must match the size of lambda
        % (otherwise we can't apply the synthesis operator)
        assert(numel(input) == lambda.size);
        output = zeros(prod(dims), 1);
    otherwise
        error('Error, invalid mode');
end

% We view the image as a piecewise constant functions whose support is
% exactly equal to [-.5, .5]^2. After these three lines, the matrix
% [x_vecs y_vecs] is a list of all of the different points of the image.
[y_vecs, x_vecs] = meshgrid(1:dims(2), 1:dims(1));
x_vecs = x_vecs(:) ./ dims(1) - 1/2;
y_vecs = y_vecs(:) ./ dims(2) - 1/2;

for num = 1 : step : lambda.size
    % Extract an ordered triple (n, x, y) from lambda
    range = num : min(num + step - 1, lambda.size);
    [n, x, y] = lambda.get(range);
    
    % Calculate the dot product between the vector (x, y) and every point
    % in the image
    dot_prods = [x_vecs y_vecs] * [x; y];
    
    % Calculate 1/||(x, y)|| unless (x,y) = (0,0) 
    inv_norms = 1 ./ sqrt(x.^2 + y.^2);
    inv_norms(~isfinite(inv_norms)) = 1;
    
    % Calculate dot_prods / ||(x,y)||
    norm_dot_prods = bsxfun(@times, dot_prods, inv_norms);
    
    % Calculate dot_prods / ||(x,y)|| - n
    sub_norm_dot_prods = bsxfun(@minus, norm_dot_prods, n);
    
    % Calculate g(dot_prods / ||(x,y)|| - n) * exp(i 2 pi dot_prods)
    tmp = g(sub_norm_dot_prods) .* exp(1i * 2 * pi * dot_prods);
    
    % Collect the output
    switch mode
        case 'analysis'
            output(range) = tmp.' * input;
        case 'synthesis'
            tmp2 = bsxfun(@times, conj(tmp), input(range).');
            output = output + sum(tmp2, 2);
        otherwise
            error('Error, invalid mode');
    end
end
end
