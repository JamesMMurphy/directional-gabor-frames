%% DDG2D Apply the discrete direction Gabor (ddg) transform to a given image with window function (g) and paramter set (lambda).
%
% Input
%    img (N x M matrix): An image to apply the discrete directional Gabor
%       transform to.
%    g (function): The window function g
%    lambda (struct): The parameter set lambda
%
% Output
%    coeffs (vector): The frame operator applied to the input.

%   Copyright 2016 Wojciech Czaja, Benjamin Manning, James Murphy, Kevin Stubbs
function coeffs = ddg2d(img, g, lambda)
if(nargin ~= 3)
    error('Error, wrong number of arguments');
end

coeffs = analysis_synthesis_op(img(:), g, lambda, size(img), 'analysis');
end
