%$ EXAMPLE_THRESHOLDING An example of thrsholding using DGS

%   Copyright 2016 Wojciech Czaja, Benjamin Manning, James Murphy, Kevin Stubbs

% Load the image
img = double(rgb2gray(imread('pears.png')));

% Downsample so things run quickly
img = img(1:6:end, 1:6:end);

% Ranges for parameters in lambda
t_range = 0;
x_range = -ceil(size(img,1)/2) : ceil(size(img,1)/2);
y_range = -ceil(size(img,2)/2) : ceil(size(img,2)/2);

% Make the parameter set
lambda = gen_lambda('square', {t_range, x_range, y_range});

% Define window function
g = @(x)(sinc(x/16).^4);

% Take the discrete directional Gabor transform of the image
disp('Starting ddg2d');
coeffs = ddg2d(img, g, lambda);
disp('Finished ddg2d');

% Eliminate 995% of the coefficents
thresh = .995;
sorted = sort(abs(coeffs));
thresh_val = sorted(ceil(length(sorted) * thresh));
coeffs(abs(coeffs) < thresh_val) = 0;

% Take the inverse tranform
disp('Starting iddg2d')
img_new = real(iddg2d(coeffs, g, lambda, size(img)));
disp('Finished iddg2d');

% See the relative error
sqrt(sum((img_new(:) - img(:)).^2)) / sqrt(sum(img(:).^2))
