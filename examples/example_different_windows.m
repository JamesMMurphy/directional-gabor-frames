%% EXAMPLE_DIFFERENT_WINDOWS An example showing how two different windows can give the same DGS

%   Copyright 2016 Wojciech Czaja, Benjamin Manning, James Murphy, Kevin Stubbs

% Load the image
img = double(rgb2gray(imread('pears.png')));

% Downsample so things run quickly
img = img(1:6:end, 1:6:end);

% Ranges for parameters in lambda
t_range = 0;
x_range = -ceil(size(img,1)/2) : ceil(size(img,1)/2);
y_range = -ceil(size(img,2)/2) : ceil(size(img,2)/2);

% Make the parameter set
lambda = gen_lambda('square', {t_range, x_range, y_range});

% Since we assume that the image is compactly supported in [-.5,.5]^2 it 
% it turns out that the discrete directional Gabor transform only depends
% on the value of g in the interval [-sqrt(2), sqrt(2)] (this can easily be
% seen via the Cauchy-Schwarz inequality). Therefore we can do the
% following...
chi = @(x)(heaviside(x + sqrt(2)) - heaviside(x - sqrt(2))); % Indicator function of [-sqrt(2), sqrt(2)]
g1 = @(x)(sinc(x/16).^4);
g2 = @(x)(g1(x) .* chi(x) + 10 .* (1 - chi(x))); % obviously not L2

% Take the discrete directional Gabor transform of the image with g1
disp('Starting first ddg2d');
coeffs1 = ddg2d(img, g1, lambda);
disp('Finished first ddg2d');

% Take the discrete directional Gabor transform of the image with g2
disp('Starting second ddg2d');
coeffs2 = ddg2d(img, g2, lambda);
disp('Finished second ddg2d');

% Take the inverse tranform with g1
disp('Starting first iddg2d')
img_new1 = real(iddg2d(coeffs1, g1, lambda, size(img)));
disp('Finished first iddg2d');

% Take the inverse tranform with g2
disp('Starting second ddg2d');
img_new2 = real(iddg2d(coeffs2, g2, lambda, size(img)));
disp('Finished iddg2d');

% See the mean squared difference
sqrt(sum((img_new1(:) - img_new2(:)).^2))
